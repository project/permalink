<?php

/**
 * Implementation of hook_views_handlers().
 */
function permalink_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'permalink') .'/includes',
    ),
    'handlers' => array(
      'permalink_handler_field_comment' => array(
        'parent' => 'views_handler_field_comment',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
 function permalink_views_data_alter(&$data) {
  // Every field in Views has a handler, here we override Views' default
  // handler -- for comment subjects -- with our own.
  $data['comments']['subject']['field']['handler'] = 'permalink_handler_field_comment';
 }
